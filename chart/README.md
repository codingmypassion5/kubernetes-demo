# Local env

## Kubernetes in Docker (kind)

### Add ingress

add to the /etc/hosts:
```
127.0.25.1 administration.ta-integrations.haufe-umantis.local
```

Create the kind cluster:
```shell
cat <<EOF | kind create cluster --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
    listenAddress: "127.0.25.1"
  - containerPort: 443
    hostPort: 443
    protocol: TCP
    listenAddress: "127.0.25.1"
EOF
```

Add the ingress controller:
```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/kind/deploy.yaml

kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s
```

## regcred
```shell
aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 242640887765.dkr.ecr.eu-central-1.amazonaws.com
kubectl delete secret regcred
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=/home/tib/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson
```

# Install/Upgrade

Make sure that the local kubectl environment points to the right cluster/namespace (`kubectl config get-contexts`). 

In order to deploy/upgrade, you have to execute the following command from the current directory:

```shell
helm upgrade -i integrations-mgmt . -v values-local.yaml
```
