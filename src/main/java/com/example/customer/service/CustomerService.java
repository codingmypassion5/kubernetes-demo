package com.example.customer.service;

import com.example.customer.dto.Customer;
import com.example.customer.entity.CustomerEntity;
import com.example.customer.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public Customer getCustomer(Integer id){
        return prepareCustomer(customerRepository.findById(id));
    }

    public Customer prepareCustomer(Optional<CustomerEntity> customerEntity){
        CustomerEntity customerEntity1 = customerEntity.get();
        Customer customer = new Customer();
        customer.setId(customerEntity1.getId());
        customer.setFirstName(customerEntity1.getFirstName());
        customer.setLastName(customerEntity1.getLastName());
        return customer;
    }

    public Stream<Customer> getCustomers(){
        return customerRepository.findAll().stream()
                .map(customer -> prepareCustomer(Optional.ofNullable(customer)))
                .collect(Collectors.toList()).stream();

    }
}
