package com.example.customer.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Customer {
    private long id;
    private String firstName;
    private String lastName;
}

