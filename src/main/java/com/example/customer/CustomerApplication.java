package com.example.customer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

//import org.h2.tools.Server;

@SpringBootApplication
@EnableTransactionManagement
//@EnableDiscoveryClient
@Slf4j
public class CustomerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerApplication.class, args);
	}

//	@Bean(initMethod = "start", destroyMethod = "stop")
//	@Profile("develop")
//	public org.h2.tools.Server h2Server() {
//		log.info("creating h2Server...");
//		Server h2Server;
//		try {
//			h2Server = org.h2.tools.Server.createTcpServer();
//		} catch (SQLException e) {
//			throw new RuntimeException("Failed to start H2 server: ", e);
//		}
//		return h2Server;
//	}
}
