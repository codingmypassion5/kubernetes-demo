package com.example.customer.repository;

import com.example.customer.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface CustomerRepository extends JpaRepository<CustomerEntity, Serializable> {
}
