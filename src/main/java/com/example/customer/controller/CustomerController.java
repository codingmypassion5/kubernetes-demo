package com.example.customer.controller;

import com.example.customer.dto.Customer;
import com.example.customer.service.CustomerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.stream.Stream;

@RequestMapping("/customers")
@RequiredArgsConstructor
@RestController
@Slf4j
public class CustomerController {

    private final CustomerService customerService;
    @GetMapping( "/{id}")
    public Customer getCustomer(@PathVariable("id") String id){
        log.info("Calling Customer endpoint for id : {}", id);
        return customerService.getCustomer(new Integer(id));
    }

    @GetMapping( )
    public Stream<Customer> getCustomers(){
        log.info("Calling Customers endpoint!");
        return customerService.getCustomers();
    }
}
